const QR = require('qrcode')
const QrScanner = require('qr-scanner'); 
QrScanner.WORKER_PATH = 'sw.js';

module.exports = (state,emitter) => {
  // generar qr
  emitter.on('genQR', usr => {
    var opts = {
      errorCorrectionLevel: 'H',
      type: 'image/jpeg',
      quality: 0.3,
      margin: 1,
      color: {
        dark:"#1c1c1c",
        light:"#ffffff"
      }
    }
    QR.toDataURL(usr.id, opts, (err,qr) => {
      if(err) console.log(`error creando qr! ${err}`)
      const link = document.createElement('a')
      // var img = document.getElementById('image')
      // img.src = url
      link.download = `${usr.nombre}_${usr.ubicacion}`
      link.href = qr
      link.click()

    })
  })

  emitter.on('scan', () => {
    if(!state.qrScanner) emitter.emit('setScanner')
    state.scanning = !state.scanning
    state.scanning 
      ? state.qrScanner.start()
      : state.qrScanner.stop()
    emitter.emit('render')
  })

  emitter.on('setScanner', () => {
      const vid = document.querySelector("#scannerFeedback")
      state.qrScanner = new QrScanner(vid, res => {
        console.log('qr id', res)
        const id = res
        const idx = state.usuarios.reduce((acc,usr,idx) => usr.id === id ? idx : acc, {})
        console.log("IDX", idx)
        //fucking 0 falsy
        if(idx || idx === 0){ 
          const usr = state.usuarios[idx]
          const str = `Nombre: ${usr.nombre} (${usr.cantidad} entradas)
Evento: ${usr.evento}
Ubicación: ${usr.ubicacion}
Vendido por ${usr.rrpp}`
          console.log(usr)
          if(!usr.asistio){
            const ok = confirm(str)
            if(ok){
              emitter.emit('setUsuario', {idx:idx, asistio:true})
            }
          }else{
            alert(`las entradas(${usr.cantidad}) de ${usr.nombre}
ya fueron escaneadas`)
          }

        }else{
          alert('usuario no de alta')
        }
      })
  })

  emitter.on('DOMContentLoaded', () => {
    if(state.auth){
      emitter.emit('setScanner')
    }
  })
}
