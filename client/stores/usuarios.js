module.exports = (state,emitter) => {
  // get usuarios
  emitter.on('getUsuarios', () => {
    fetch('/usuarios', {method:'GET'})
      .then(res => {
        if(!res.ok)console.log('error pidiendo mensajes!')
        res.json().then(data => {
          state.usuarios = data
          emitter.emit('render')
        }).catch(err => console.log(`error parseando json! ${err}`))
      })
      .catch(err => console.log(`error obteniendo recurso ${err}`))
  })
  const validForm = f => f.nombre !== '' && f.ubicacion !== '' && f.cantidad !== '' && f.email != ''

  // new usuario
  emitter.on('nuevoUsuario', form => {
    console.log('form', form)
    //emitter.emit('genQR')
    var data = new FormData(form)
    var headers = new Headers({ 'Content-Type': 'application/json' })      
    var body = {}
    for(let pair of data.entries()) body[pair[0]] = pair[1]
    if(validForm(body)){
      body = JSON.stringify(body)
      fetch('/nuevoUsuario', {method:'POST', body, headers})
        .then(res => {
          if(!res.ok) console.log('error haciendo post')
          emitter.emit('getUsuarios') 
          emitter.emit('render')
        })
        .catch(err => console.log(`error dando de alta a usuario ${err}`))
    }
  })

  //borrar usuario
  emitter.on('borrarUsuario', idx => {
    var headers = new Headers({ 'Content-Type': 'application/json' })      
    var body = {idx : idx}
    body = JSON.stringify(body)     
    fetch('/borrarUsuario', {method:'POST', body, headers})
      .then(res => {
        if(!res.ok)console.log('error enviando data!')
        emitter.emit('getUsuarios')
      })
      .catch(err => console.log(`error eliminando usuario ${err}`))
  })

  emitter.on('borrarUsuarios', _ => {
    fetch('/borrarUsuarios')
      .then(res => {
        if(!res.ok) console.log('error limpiando la db!')
        emitter.emit('getUsuarios')
      })
      .catch(err => console.log(`error limpiando la db ${err}`))
  })


  emitter.on('setUsuario', ({idx,asistio}) => {
    var headers = new Headers({ 'Content-Type': 'application/json' })      
    var body = {idx : idx,  asistio: asistio}
    body = JSON.stringify(body)     
    fetch('/setUsuario', {method:'POST', headers,body})
      .then(res => {
        if(!res.ok)console.log('error enviando estado de usuario')
      })
      .catch(err => console.log(`error enviando estado de usuario ${err}`))
    emitter.emit('getUsuarios')
  })

}
