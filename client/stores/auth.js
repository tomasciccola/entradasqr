module.exports = (state,emitter) => {
  state.auth = Boolean(window.localStorage.getItem('auth'))
  state.is_admin = Boolean(window.localStorage.getItem('admin'))

  if(state.auth) emitter.emit('getUsuarios')


  emitter.on('auth', (form) => {
    var data = new FormData(form)
    var headers = new Headers({ 'Content-Type': 'application/json' })      
    var body = {}
    for(let pair of data.entries()) body[pair[0]] = pair[1]
    body = JSON.stringify(body)
    fetch('/auth', {method:'POST', body,headers})
      .then(res => {
        if(!res.ok && res.status === 401) {
          console.log('error haciendo post')
          return alert('contraseña incorrecta')
        }
        if(res.ok && res.status === 200){
          res.json().then(json => {
            state.auth = true
            state.is_admin = json.is_admin 
            window.localStorage.setItem('auth', state.auth)
            window.localStorage.setItem('admin', state.is_admin)
            emitter.emit('getUsuarios')
            emitter.emit('render')
          })
        }
      })
  })
}
