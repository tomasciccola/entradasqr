const choo = require('choo')
const html = require('choo/html')
const css = require('sheetify')
const app = choo()

css('tachyons')

app.use(require('choo-devtools')())
app.use(require('choo-service-worker')())

// stores
app.use(require('./stores/usuarios.js'))
app.use((state, emitter) => {
  state.usuarios = []
  state.scanning = false
  //emitter.emit('getUsuarios')
})
app.use(require('./stores/qr.js'))
app.use(require('./stores/auth.js'))

// views
const usuarioForm = require('./views/usuario.js')
const renderUsuarios = require('./views/usuarios.js')
const authView = require('./views/auth.js')

const mainView = (state, emit) => {
  const scan = _ => emit('scan')

  return html`
    <body class="pa3 code bg-near-black silver w-100">
    <div class="flex flex-column center w-100">
      <button class="f2 pa2 w-80 center"
        onclick=${scan}>scannear QR</button>
      <video class="w-100"
        style="display: ${state.scanning ? "block" : "none"}"
        id="scannerFeedback">
      </video>
    </div>
    <div id="scanner"></div>
    ${usuarioForm(state, emit)}
    <div class="flex center flex-column justify-between">
      <p>cantidad de entradas vendidas: ${state.usuarios.length || 0}</p>
    </div>
    <div id="usuarios">
      ${state.usuarios.map(renderUsuarios(state, emit))}
    </div>
  </body>
  `
}

app.route('/', (state, emit) => {
  return state.auth ? mainView(state, emit) : authView(state, emit)
})

app.mount('body')
