const css = require('sheetify')
const html = require('choo/html')

module.exports = (state,emit) => (usuario, idx) => {

  const eliminar = () => {
    if(confirm(`¿estás segurx de eliminar a este usuarix?`)){
      emit('borrarUsuario', idx)
    }
  }

  const genQR = () => {
    emit('genQR', usuario)
  }

  const unconfirm = () => {

    const cancel = () => {
      if(confirm(`¿estás seguro de desredimir las entradas de este usuarix?`))
      emit('setUsuario', {idx:idx, asistio:false})
    }

    return html`
      <div class="w4">
        <span class="mr2">✓</span>
        <button class="silver bg-near-black b-silver br4" onclick=${cancel}>x</button>
      </div> `
  }

  return html`
  <div id=usuario_${idx}>
    <div 
    class="flex flex-row w-100 justify-between items-center center mt2 f4 h1">
      <button class="w2 mr3" onclick=${genQR}>q</button>
      <p class="w5">${usuario.nombre} </p>
      ${usuario.asistio ? unconfirm() : html`<p class="w3">✗</p>`  }
      <button class="w2" onclick=${eliminar}>X</button>
    </div>
    <div class="flex flex-row w-100 justify-around items-center center f4">
      <p class="w-30">${usuario.evento} </p>
      <p class="w-40"> ${usuario.ubicacion} (${usuario.cantidad}) </p>
      ${state.is_admin 
      ? html `<p class="w-30">${usuario.rrpp} </p>` 
      : html `<p></p>`}
    </div>
    <hr>
  </div>
  `
}
