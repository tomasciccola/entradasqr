const html = require('choo/html')
const css = require('sheetify')
css('tachyons')

module.exports = (state,emit) => {
  const submit = e => {
    e.preventDefault()
    emit('auth', e.currentTarget)
  }

  return html`
  <body class="pa3 code bg-near-black silver w-100 h-75">
    <form class="f3 mv7 w-50 center flex flex-column justify-around" id="auth" onsubmit=${submit}>
      <label class="tc" for="pass"> Contraseña </label>
      <input id="pass" name="pass" type="password" />
      <input type="submit" value="enviar" />
    </form>
  </body>
  `

}
