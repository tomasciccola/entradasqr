const html = require('choo/html')
const css = require('sheetify')
css('tachyons')

module.exports = (state,emit) => {
  const submit = e => {
    e.preventDefault()
    emit('nuevoUsuario', e.currentTarget)
  }

  const borrarUsuarios = e => {
    //e.preventDefault()
    if(confirm("ojo! vas a eliminar toda la base de usuarios, ¿Estás segurx?")){
      emit('borrarUsuarios')
    }
  }

  return html`
    <form class="flex center bb bw1 pb2 flex-column justify-between w-80" id="formularioUsuario" onsubmit=${submit}>
      <div class="w-100 pb2 mb2 bw1 bb flex flex-column justify-around">
        <h3 class="w-800 f2 w-100 center tc"> Carga de datos </h3> 
        <div class="flex flex-column w-80 ml2 self-center">
        <input class="pa1 f4 self-center w-100" type="submit" value="enviar" />
        <button class="self-center tc ma8 f4 w-100 pa1" onclick=${borrarUsuarios}>eliminar todo</button>
        </div>
      </div>
      <div class="w-100 flex flex-column justify-evenly">
        <label class="w-100 f2 self-center" for="nombre"> Nombre </label>
        <input class="w-100 f2" id="nombre" name="nombre" type="text" />
      </div>
      <div class="w-100 flex flex-column justify-evenly">
        <label class="w-100 f2 self-center" for="email"> Email </label>
        <input class="w-100 f2" id="email" name="email" type="text" />
      </div>
      <div class="flex w-100 flex-column mv2">
        <label class="w-100 f2 self-center" for="ubicacion"> Ubicación </label>
        <input class="w-100 f2" id="ubicacion" name="ubicacion" type="text" />
      </div>
      <div class="flex w-100 flex-column mv2">
        <label class="w-100 f2 self-center" for="cantidad"> Cantidad </label>
        <input class="w-100 f2" id="cantidad" name="cantidad" type="number" />
      </div>
      <div class="flex w-100 flex-column mv2">
        <label class="w-100 f2 self-center" for="evento"> Evento </label>
        <input class="w-100 f2" id="evento" name="evento" type="text" />
      </div>
      <div class="flex w-100 flex-column mv2">
        <label class="w-100 f2 self-center" for="rrpp"> RRPP </label>
        <input class="w-100 f2" id="rrpp" name="rrpp" type="text" />
      </div>
    </form>
  `
}

