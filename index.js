require('dotenv').config()

const https = require('https')
const http = require('http')
const path = require('path')
const fs = require('fs')
const { createHash } = require('crypto')
const setHeaders = (res,path) => {
  console.log('path', path)
  res.setHeader('Content Type', 'text/javascript')
}
const serveStatic = require('serve-static', {setHeaders});
const finalhandler = require('finalhandler');
const serve = serveStatic("client/dist");
// const bankai = require('bankai/http')
const uuid = require('uuid').v4
const Papa = require('papaparse')
const QR = require('qrcode')
const sendEmail = require('./sendemail.js')

//aws bucket
const AWS = require('aws-sdk')
const BUCKET = 'entradas-qr'
const s3Params = {
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
}
const s3 = new AWS.S3(s3Params)

const PORT = process.env.PORT || 8080

var db = {}

const getDb = () => {
  const options = {
    Bucket: BUCKET,
    Key: 'usuarios.csv'
  }
  s3.getObject(options, (err,res) => {
    if(err)return console.log('error obteniendo db!', err)
    const d = Papa.parse(res.Body.toString(), {header:true, dynamicTyping:true})
    db = d.data
  })
}
getDb()

//load certs
const key = fs.readFileSync(path.join(__dirname, 'cert', 'cert.key'))
const cert = fs.readFileSync(path.join(__dirname, 'cert', 'cert.crt'))
const credentials = {key:key, cert:cert}

// var compiler = bankai(path.join(__dirname, 'client/index.js'))


const hash = (input) => {
  return createHash('sha256').update(input).digest('hex')
}

// para crear la pass
/*
const str = `KEY=${hash('isagorra2023')}`
process.stdout.write(str)
process.exit(0)
*/

const parsePost = (req,onEnd) =>{
  let body = ''
  req.on('data', chunk =>{
    body += chunk
  })
  req.on('end', () =>{
    onEnd(JSON.parse(body))
  })
}

const getFileTimestamp = () => {
  const d = new Date()
  return d.toUTCString()
    .split(',')[1]
    .replace(/:/g, '_')
    .replace(/ /g, '_')
}


const updateDb = (cb) => {
  // console.log('db', db)
  var csv = Papa.unparse(db, {dynamicTyping:true});
  //console.log(csv)
  const params = {
    Bucket:BUCKET,
    Key: 'usuarios.csv',
    ContentType:'text/csv',
    Body: csv
  }

  s3.upload(params, (err,res) => {
    if(err) console.log('error', err)
    console.log('subido con éxito!')
    cb()
  })

  //fs.writeFileSync('./data/usuarios.json', JSON.stringify(db,null,4))
}

const generateQR = ({qr, id}, cb) => {
  var opts = {
    errorCorretionLevel:'H',
    type:'image/png',
    quality:0.3,
    margin:1,
    color: {
      dark:"#1c1c1c",
      light:"#ffffff"
    }
  }
  QR.toFile(qr, id, opts, cb)
}

const handler = (req,res) => {
  console.log(req.method, req.url)

  if(req.url === '/usuarios' && req.method === 'GET') {

    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify(db))

  }else if(req.url === '/auth' && req.method === 'POST') {
    console.log('auth!')
    parsePost(req, json => {
      const pass = json.pass
      const is_admin = hash(pass) === process.env.ADMIN_KEY
      const is_rrpp = hash(pass) === process.env.KEY
      if(is_rrpp || is_admin){
        res.statusCode = 200
        res.ok = true
        res.end(JSON.stringify({is_admin}))
      }else{
        res.statusCode = 401
        res.ok = false
        res.end('not ok')
      }
    })
  }else if(req.url === '/nuevoUsuario' && req.method === 'POST') {

    parsePost(req, json => {
      console.log('nuevo usuario!', json)
      //creas id unico y booleano de asistió
      json.id = uuid()
      json.asistio = false
      // generas el qr
      json.qr = `/tmp/${json.id}.png`
      generateQR(json, (err,qr) => {
        if(err)return console.log('error generando qr', err)
        // send mail
        sendEmail(json, (err,d) => {
          if(err) return console.log("error enviando mail", err)
          console.log('mail enviado!', d.response)
          db.push(json)
          updateDb(() => {
            res.end('ok!')
          })
        })
      })
    })

  }else if(req.url === '/setUsuario' && req.method === 'POST'){
    parsePost(req, json => {
      console.log('seteando usuario', db[json.idx], json.asistio)
      db[json.idx].asistio = json.asistio
      updateDb(() => {
        res.end('ok')
      })
    })
  }else if(req.url === '/borrarUsuario' && req.method === 'POST'){

    parsePost(req,json => {
      console.log('usuario a eliminar', db[json.idx])
      db.splice(json.idx,1)
      updateDb(() => {
        res.end('ok')
      })
    })

  }else if(req.url === '/borrarUsuarios' && req.method === 'GET'){

    console.log('borrando db!')
    // const p = path.join('data', 'backup',`db${getFileTimestamp()}.json` )
    // fs.writeFileSync(p, JSON.stringify(db, false, 4))
    db.length = []
    updateDb(() => res.end('ok'))

  }else if(req.url === '/node_modules/qr-scanner-worker.min.js' && req.method === 'GET'){
    const p = path.join(__dirname, 'node_modules', 'qr-scanner-worker.min.js')
    res.end(fs.readFileSync(p))
  }else{
    serve(req,res,finalhandler(req,res))
  }
}

// const serverS = https.createServer(credentials, handler)
const server = http.createServer(handler)

// serverS.listen(PORT+1, () => {
//   console.log(`servidor seguro escuchando en ${PORT+1}`)
// })

server.listen(PORT, () =>{
  console.log(`servidor escuchando en ${PORT}`)
})
