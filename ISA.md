# Tareas

## CREAR UNA CUENTA DE GMAIL
* Si te pregunta, no habilitar la verificación en dos pasos (2FA), por las dudas
* Crear una contraseña de la aplicación (https://support.google.com/accounts/answer/185833?hl=es-419)
  Esa contraseña te la crea automáticamente google. Guardarla y pasarmela a mi junto con el mail creado.

## CREAR UNA CUENTA EN AWS
* Crearla acá https://aws.amazon.com/
* Tomar nota de la contraseña y pasármela. Te va a pedir que pongas una tarjeta de crédito.
* Si querés me podés pasar esa contraseña y yo me ocupo del resto. Si no confías en que tenga acceso a la cuenta (porque tiene asociada una tarjeta de crédito), 
avisame y te doy más instrucciones.

