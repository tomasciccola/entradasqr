const nodemailer = require('nodemailer')
let transporter = nodemailer.createTransport({
  service:'gmail',
  auth: {
    user:process.env.MAIL_USER,
    pass:process.env.MAIL_PASS
  }
})

module.exports = function({nombre, email, ubicacion, evento, cantidad, qr}, sended){
  let opts = {
    from: `Rave parties corp <${process.env.MAIL_USER}>`,
    to: email,
    subject: `Tu entrada para ${evento}`,
    html:`
    <h2>Hola ${nombre}, </h2>
    <p>
    Acabás de adquirir ${cantidad} ticket/s para ${evento} en el sector ${ubicacion}
    </p>
    <p>Te dejamos un código QR que oficiará como medio de ingreso al evento. Recordá tenerlo a mano (en tu teléfono o impreso) para poder acceder al show. Cualquier duda luego de adquirir tu ticket nos podés escribir por mensaje directo a nuestro perfil de Instagram: @ravepartiescorp</p>
    ¡Gracias por confiar en nosotros, nos vemos ahí!
    `, 
    attachments: [{
      filename:'qr.png',
      path:qr,
      cid:qr
    }]
  }
  transporter.sendMail(opts, sended)
}
